Snaiks Demo 2
=====

This is a short demonstraition of the Snaiks poroject.  
http://zeilhofer.co.at/snaiks

# Schematic Design
This schematic was created with [KiCads](http://kicad-pcb.org/) schematic editor, using the [Snaiks KiCad Library](https://gitlab.com/KarlZeilhofer/snaiks-kicad-lib). 

It implements a running light, with 2 switches to trigger the sequence. 

![schematic](/2017-03-21_005.png "Snaiks Schematic")  
All the timing and logic is defined and documented here. 

# Runtime
The logic defined with the schematic is then compiled with the [Snaiks Compiler](https://gitlab.com/KarlZeilhofer/snaiks-compiler) with this command:  
`snaiksc snaiks-demo-2.net > snaikscontroller.h`  
into the the C++ header file `snaikscontroller.h`. This headerfile can be used in any C++ project. Note that `snaiksc` must be installed on your system. 

## Console
A simple console application demonstrates the logic.  
![Snaiks on Console](/2017-03-21_007.png "Snaiks on console")  

## Qt
For testing the logic on a PC, I've wirtten a simple GUI, which represents the states of the inputs and outputs.  
![Snaiks on Qt](/2017-03-21_006.png "Snaiks on Qt")  

## RevolutionPi
There is also an example, that runs on the [RevolutionPi from Kunbus](https://revolution.kunbus.de/). 

![Snaiks on RevolutionPi](/revpi-setup.jpg "Snaiks on RevolutionPi")  

# Workflow
* draw your schematic, using the snaiks-kicad-lib
* annotate the schematic
* run the ERC, to check for errors in KiCad
* export the netlist-file (*.net)
* compile the netlist file into snaikscontroller.h (C++)
* copy this file into your C++ project
* recompile your snaiks project in C++
* start the application
