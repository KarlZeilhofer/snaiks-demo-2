/**
 * Snaiks Demo 2
 * This Demo shows the usage of a simple function block diagram,
 * designed with Snaiks running on a RevolutionPi with an attached
 * DIO Module from Kunbus.
 *
 * Setup:
 * DIO Module is to the left of the Core Module, Offsets: In=0, Out=70
 * In1 and In2 are connected to active high tacticle switches.
 * Out1-4 are connected to LEDs.
 *
 * You can compile this project by running
 *   make
 * or
 *   make nosnaiks
 * which compiles only this directory, running an outputs test only.
 *
 * For further information about Snaiks, please visit www.zeilhofer.co.at/snaiks
 * (c) Karl Zeilhofer, karl@zeilhofer.co.at
 * License: GPL v3
 */



#include <iostream>
#include <stdlib.h> // for exit()

#ifndef NOSNAIKS
#include "controller.h"
#include <Snaiks.h>
#endif

#include <piControlIf.hpp>

using namespace std;


// get this values with piTest -d for your configuration:
#define IN_OFFSET 0
#define OUT_OFFSET 70


// Global variables:
#ifndef NOSNAIKS
Controller* snaiks;
#endif
piControl revpi;

bool In1, In2;
bool *Out1, *Out2, *Out3, *Out4; // NOTE: using bool-pointer as outputs, since we connect them to Snaiks

// from piTest.c, written by Kunbus
const char* const getReadError(int error)
{
	static const char * const ReadError[] =
	{
		"Cannot connect to control process",
		"Offset seek error",
		"Cannot read from control process",
		"Unknown error"
	};
	switch (error)
	{
	case -1:
		return ReadError[0];
		break;
	case -2:
		return ReadError[1];
		break;
	case -3:
		return ReadError[2];
		break;
	default:
		return ReadError[3];
		break;
	}
}

// from piTest.c, written by Kunbus
const char* const getWriteError(int error)
{
	static const char * const WriteError[] =
	{
		"Cannot connect to control process",
		"Offset seek error",
		"Cannot write to control process",
		"Unknown error"
	};
	switch (error)
	{
	case -1:
		return WriteError[0];
		break;
	case -2:
		return WriteError[1];
		break;
	case -3:
		return WriteError[2];
		break;
	default:
		return WriteError[3];
		break;
	}
}

#ifndef NOSNAIKS
void setup()
{
	cout << "setup snaiks...";
	snaiks->GlobalSampleTime = 0.05;
	snaiks = new Controller(1, "SnaiksDemo2");
	cout << "done" << endl;
}

void connectToSnaiks()
{
	cout << "connectToSnaiks...";
	snaiks->getObj(Sn::DIN, 1)->getPin(51)->setSource(&In1);
	snaiks->getObj(Sn::DIN, 2)->getPin(51)->setSource(&In2);
	Out1 = (bool*) snaiks->getObj(Sn::DOUT, 1)->getPin(11)->outputAddress();
	Out2 = (bool*) snaiks->getObj(Sn::DOUT, 2)->getPin(11)->outputAddress();
	Out3 = (bool*) snaiks->getObj(Sn::DOUT, 3)->getPin(11)->outputAddress();
	Out4 = (bool*) snaiks->getObj(Sn::DOUT, 4)->getPin(11)->outputAddress();
	cout << "done" << endl;
}
#endif

// read values from the RevolutionPi Process Image
void read()
{
	SPIValue v;
	int rc;

	v.i16uAddress = IN_OFFSET; // Inputs of DIO-Module
	v.i8uBit = 0;
	rc = revpi.GetBitValue(&v);
	if(rc < 0){
		cout << getReadError(rc) << endl;
		exit(1);
	}
	In1 = v.i8uValue;

	v.i8uBit = 1;
	rc = revpi.GetBitValue(&v);
	if(rc < 0){
		cout << getReadError(rc) << endl;
		exit(1);
	}
	In2 = v.i8uValue;
}

// write values to the RevolutionPi Process Image
void write()
{
	SPIValue v;

	int rc;

	v.i16uAddress = OUT_OFFSET; // Outputs of DIO-Module
		v.i8uBit = 0;
	v.i8uValue = *Out1;
	rc = revpi.SetBitValue(&v);
	if(rc < 0){
		cout << getWriteError(rc) << endl;
		exit(1);
	}

	v.i8uBit = 1;
	v.i8uValue = *Out2;
	rc = revpi.SetBitValue(&v);
	if(rc < 0){
		cout << getWriteError(rc) << endl;
		exit(1);
	}

	v.i8uBit = 2;
	v.i8uValue = *Out3;
	rc = revpi.SetBitValue(&v);
	if(rc < 0){
		cout << getWriteError(rc) << endl;
		exit(1);
	}

	v.i8uBit = 3;
	v.i8uValue = *Out4;
	rc = revpi.SetBitValue(&v);
	if(rc < 0){
		cout << getWriteError(rc) << endl;
		exit(1);
	}
}

int millis(){
	static clock_t start;

	static bool firstRun=true;
	if(firstRun){
		firstRun = false;
		start = clock();
	}

	clock_t now = clock();
	clock_t diff = now-start;

	double t = (double)diff/(CLOCKS_PER_SEC*0.666666); // workaround to timing bug on RevPi
	// see https://revolution.kunbus.de/forum/viewtopic.php?f=6&t=175#p573

	return t*1000;
}

void delay(int milliseconds)
{
	int start = millis();
	while(millis() < start+milliseconds);
}

#ifndef NOSNAIKS
void loop()
{
	static int lastMillis = 0;
	int now = millis();

	if(now >= (lastMillis+snaiks->GlobalSampleTime*1000)){
		read();
		snaiks->sample();
		snaiks->update();
		write();

		lastMillis = now;
	}
}

void SnaiksErrorCallback(const char* const msg,
						 const char* const file, int line)
{
	cout << "Snaiks Error: " << msg << ", in file " << file << ":" << line << endl;
}
#endif

void testOutputs()
{
	SPIValue v;

	cout << "testOutputs()" << endl;

	v.i16uAddress = OUT_OFFSET;

	v.i8uBit = 0;
	v.i8uValue = 1;
	cout << "Out1 = ON" << endl;
	revpi.SetBitValue(&v);
	delay(500);
	v.i8uValue = 0;
	cout << "Out1 = OFF" << endl;
	revpi.SetBitValue(&v);

	v.i8uBit = 1;
	v.i8uValue = 1;
	cout << "Out2 = ON" << endl;
	revpi.SetBitValue(&v);
	delay(500);
	v.i8uValue = 0;
	cout << "Out2 = OFF" << endl;
	revpi.SetBitValue(&v);

	v.i8uBit = 2;
	v.i8uValue = 1;
	cout << "Out3 = ON" << endl;
	revpi.SetBitValue(&v);
	delay(500);
	v.i8uValue = 0;
	cout << "Out3 = OFF" << endl;
	revpi.SetBitValue(&v);

	v.i8uBit = 3;
	v.i8uValue = 1;
	cout << "Out4 = ON" << endl;
	revpi.SetBitValue(&v);
	delay(500);
	v.i8uValue = 0;
	cout << "Out4 = OFF" << endl;
	revpi.SetBitValue(&v);
}


int main(int argc, char *argv[])
{
	(void)argc; (void)argv;


	cout << "Snaiks Demo 2" << endl;

	testOutputs();

#ifndef NOSNAIKS
	setup();
	connectToSnaiks();

	cout << "run.." << endl;
	while(1){
		loop();
	}
#endif

	return 0;
}
