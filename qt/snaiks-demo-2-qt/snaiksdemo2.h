#ifndef WINDOW_H
#define WINDOW_H

#include <QWidget>
#include <QTimer>
#include "controller.h"

namespace Ui {
class Window;
}


class SnaiksInputs
{
public:
	void read();
	bool In1;
	bool In2;
	// NOTE: you can add your project specific pins here
};

class SnaiksOutputs
{
public:
	void write();
	bool* Out1;
	bool* Out2;
	bool* Out3;
	bool* Out4;
	// NOTE: you can add your project specific pins here
};



class SnaiksDemo2 : public QWidget
{
	Q_OBJECT

public:
	friend class SnaiksInputs;
	friend class SnaiksOutputs;

	explicit SnaiksDemo2(QWidget *parent = 0);
	~SnaiksDemo2();

	void unitTests();
	void setup();
	void connectToSnaiks();
public slots:
	void loop();

public: // Objects
	SnaiksInputs in;
	SnaiksOutputs out;
	Controller* snaiks;

private:
	Ui::Window *ui;
	QTimer timer;
};

extern SnaiksDemo2* app;


#endif // WINDOW_H
