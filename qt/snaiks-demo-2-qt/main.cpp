#include "snaiksdemo2.h"
#include <QApplication>
#include "qdebug.h"

SnaiksDemo2* app;

int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
	SnaiksDemo2 w;
	app = &w;
	w.show();

	return a.exec();
}

void SnaiksErrorCallback(const char* const msg,
						 const char* const file, int line)
{
	qDebug() << "Snaiks Error: " << msg << "in file " << file << ":" << line;
}
