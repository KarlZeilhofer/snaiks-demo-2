QT += core
QT += widgets
#QT -= gui

CONFIG += c++11

TARGET = snaiks-demo-2-qt
CONFIG -= app_bundle

TEMPLATE = app

SOURCES += main.cpp \
	snaiksdemo2.cpp \
    ../../snaiks-cpp-lib/abs.cpp \
    ../../snaiks-cpp-lib/ad.cpp \
    ../../snaiks-cpp-lib/ain.cpp \
    ../../snaiks-cpp-lib/aout.cpp \
    ../../snaiks-cpp-lib/aud.cpp \
    ../../snaiks-cpp-lib/bessel4_.cpp \
    ../../snaiks-cpp-lib/cb.cpp \
    ../../snaiks-cpp-lib/cmp.cpp \
    ../../snaiks-cpp-lib/damx.cpp \
    ../../snaiks-cpp-lib/dand.cpp \
    ../../snaiks-cpp-lib/ddmx.cpp \
    ../../snaiks-cpp-lib/dif.cpp \
    ../../snaiks-cpp-lib/din.cpp \
    ../../snaiks-cpp-lib/div.cpp \
    ../../snaiks-cpp-lib/dl.cpp \
    ../../snaiks-cpp-lib/dor.cpp \
    ../../snaiks-cpp-lib/dout.cpp \
    ../../snaiks-cpp-lib/dud.cpp \
    ../../snaiks-cpp-lib/ff.cpp \
    ../../snaiks-cpp-lib/hld.cpp \
    ../../snaiks-cpp-lib/int.cpp \
    ../../snaiks-cpp-lib/max.cpp \
    ../../snaiks-cpp-lib/memorydispenser.cpp \
    ../../snaiks-cpp-lib/mf.cpp \
    ../../snaiks-cpp-lib/min.cpp \
    ../../snaiks-cpp-lib/mul.cpp \
    ../../snaiks-cpp-lib/ps.cpp \
    ../../snaiks-cpp-lib/qand.cpp \
    ../../snaiks-cpp-lib/qor.cpp \
    ../../snaiks-cpp-lib/sat.cpp \
    ../../snaiks-cpp-lib/sc.cpp \
    ../../snaiks-cpp-lib/sign.cpp \
    ../../snaiks-cpp-lib/snobject.cpp \
    ../../snaiks-cpp-lib/snpin.cpp \
    ../../snaiks-cpp-lib/snproperty.cpp \
    ../../snaiks-cpp-lib/snunittest.cpp \
    ../../snaiks-cpp-lib/srl.cpp \
    ../../snaiks-cpp-lib/st.cpp \
    ../../snaiks-cpp-lib/sum.cpp \
    ../../snaiks-cpp-lib/timer.cpp \
    ../../snaiks-cpp-lib/trig.cpp

HEADERS += \
    controller.h \
    snaikscontroller.h \
	snaiksdemo2.h \
    ../../snaiks-cpp-lib/abs.h \
    ../../snaiks-cpp-lib/ad.h \
    ../../snaiks-cpp-lib/ain.h \
    ../../snaiks-cpp-lib/aout.h \
    ../../snaiks-cpp-lib/aud.h \
    ../../snaiks-cpp-lib/bessel4_.h \
    ../../snaiks-cpp-lib/cb.h \
    ../../snaiks-cpp-lib/cmp.h \
    ../../snaiks-cpp-lib/config.h \
    ../../snaiks-cpp-lib/damx.h \
    ../../snaiks-cpp-lib/dand.h \
    ../../snaiks-cpp-lib/ddmx.h \
    ../../snaiks-cpp-lib/dif.h \
    ../../snaiks-cpp-lib/din.h \
    ../../snaiks-cpp-lib/div.h \
    ../../snaiks-cpp-lib/dl.h \
    ../../snaiks-cpp-lib/dor.h \
    ../../snaiks-cpp-lib/dout.h \
    ../../snaiks-cpp-lib/dud.h \
    ../../snaiks-cpp-lib/ff.h \
    ../../snaiks-cpp-lib/hld.h \
    ../../snaiks-cpp-lib/int.h \
    ../../snaiks-cpp-lib/max.h \
    ../../snaiks-cpp-lib/memorydispenser.h \
    ../../snaiks-cpp-lib/mf.h \
    ../../snaiks-cpp-lib/min.h \
    ../../snaiks-cpp-lib/mul.h \
    ../../snaiks-cpp-lib/ps.h \
    ../../snaiks-cpp-lib/qand.h \
    ../../snaiks-cpp-lib/qor.h \
    ../../snaiks-cpp-lib/sat.h \
    ../../snaiks-cpp-lib/sc.h \
    ../../snaiks-cpp-lib/sign.h \
    ../../snaiks-cpp-lib/Snaiks.h \
    ../../snaiks-cpp-lib/sncontainer.h \
    ../../snaiks-cpp-lib/snerrorcallback.h \
    ../../snaiks-cpp-lib/snobject.h \
    ../../snaiks-cpp-lib/snpin.h \
    ../../snaiks-cpp-lib/snproperty.h \
    ../../snaiks-cpp-lib/snpropertyinterface.h \
    ../../snaiks-cpp-lib/snunittest.h \
    ../../snaiks-cpp-lib/srl.h \
    ../../snaiks-cpp-lib/st.h \
    ../../snaiks-cpp-lib/sum.h \
    ../../snaiks-cpp-lib/timer.h \
    ../../snaiks-cpp-lib/trig.h

INCLUDEPATH += ../../snaiks-cpp-lib

DISTFILES += \
    ../../snaiks-cpp-lib/README.md


FORMS += \
    window.ui
