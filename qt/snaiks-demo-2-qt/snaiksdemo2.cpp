#include "snaiksdemo2.h"
#include "ui_window.h"
#include <snunittest.h>
#include "qdebug.h"

SnaiksDemo2::SnaiksDemo2(QWidget *parent) :
	QWidget(parent),
	ui(new Ui::Window)
{
	ui->setupUi(this);

	snaiks->GlobalSampleTime = 0.05; // 50ms

	timer.setInterval(snaiks->GlobalSampleTime*1000);

	connect(&timer, SIGNAL(timeout()), this, SLOT(loop()));

	timer.start();

	app = this;
}

SnaiksDemo2::~SnaiksDemo2()
{
	delete ui;
}

void SnaiksDemo2::unitTests()
{
	qDebug() << "unitTests()";
	SnUnitTest::run();
}

void SnaiksDemo2::setup()
{
	qDebug() << "setup()";
	snaiks = new Controller(1, "Snaiks-Demo-2");
	connectToSnaiks();
	in.In1 = 0;
	in.In2 = 0;
	SnObject::GlobalSampleTime = ((snanalog_t)timer.interval())/1000;
}

void SnaiksDemo2::connectToSnaiks()
{
	qDebug() << "connectToSnaiks()";
	snaiks->getObj(Sn::DIN, 1)->getPin(51)->setSource(&in.In1);
	snaiks->getObj(Sn::DIN, 2)->getPin(51)->setSource(&in.In2);
	out.Out1 = (bool*) snaiks->getObj(Sn::DOUT, 1)->getPin(11)->outputAddress();
	out.Out2 = (bool*) snaiks->getObj(Sn::DOUT, 2)->getPin(11)->outputAddress();
	out.Out3 = (bool*) snaiks->getObj(Sn::DOUT, 3)->getPin(11)->outputAddress();
	out.Out4 = (bool*) snaiks->getObj(Sn::DOUT, 4)->getPin(11)->outputAddress();
}

// called by timer
void SnaiksDemo2::loop()
{
	static bool firstRun = true;
	if(firstRun){
		firstRun = false;

		unitTests();
		setup();
		qDebug() << "run...";
	}else{
		in.read();
		snaiks->sample();
		snaiks->update();
		out.write();
	}
}

void SnaiksOutputs::write()
{
	app->ui->checkBox_Out1->setChecked(*Out1);
	app->ui->checkBox_Out2->setChecked(*Out2);
	app->ui->checkBox_Out3->setChecked(*Out3);
	app->ui->checkBox_Out4->setChecked(*Out4);
}

void SnaiksInputs::read()
{
	In1 = app->ui->pushButton_In1->isChecked();
	In2 = app->ui->pushButton_In2->isChecked();
}

