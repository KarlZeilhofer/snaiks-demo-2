#include <iostream>
#include "controller.h"
#include <Snaiks.h>
#include <time.h>


using namespace std;


// Global variables:
Controller* snaiks;
bool In1, In2;
bool *Out1, *Out2, *Out3, *Out4;

void setup()
{
	cout << "setup snaiks...";
	snaiks = new Controller(1, "SnaiksDemo2");
	snaiks->GlobalSampleTime = 0.1;
	cout << "done" << endl;
}

void connectToSnaiks()
{
	cout << "connectToSnaiks()" << endl;
	snaiks->getObj(Sn::DIN, 1)->getPin(51)->setSource(&In1);
	snaiks->getObj(Sn::DIN, 2)->getPin(51)->setSource(&In2);
	Out1 = (bool*) snaiks->getObj(Sn::DOUT, 1)->getPin(11)->outputAddress();
	Out2 = (bool*) snaiks->getObj(Sn::DOUT, 2)->getPin(11)->outputAddress();
	Out3 = (bool*) snaiks->getObj(Sn::DOUT, 3)->getPin(11)->outputAddress();
	Out4 = (bool*) snaiks->getObj(Sn::DOUT, 4)->getPin(11)->outputAddress();
}

int millis(){
	static clock_t start;

	static bool firstRun=true;
	if(firstRun){
		firstRun = false;
		start = clock();
	}

	clock_t now = clock();
	clock_t diff = now-start;

	double t = (double)diff/CLOCKS_PER_SEC;

	return t*1000;
}

void loop()
{
	static int lastMillis = 0;

	int now = millis();

	if(now >= (lastMillis+snaiks->GlobalSampleTime*1000)){
		snaiks->sample();
		snaiks->update();
		lastMillis = now;
	}
}

void SnaiksErrorCallback(const char* const msg,
						 const char* const file, int line)
{
	cout << "Snaiks Error: " << msg << ", in file " << file << ":" << line << endl;
}


int main(int argc, char *argv[])
{
	(void)argc; (void)argv;

	cout << "Snaiks Demo 2" << endl;

	setup();
	connectToSnaiks();

	In1 = In2 = false;


	int i=0;
	while(millis() < 10000){
		if(millis()/100 != i){
			i = millis()/100;
			cout << "t = " << i << endl;

			if(i==1){
				cout << "PingPong Pong #####################" << endl;
				In1 = In2 = true;
			}
			if(i==35){
				cout << "PingPong ##########################" << endl;
				In1 = true;
			}
			if(i==70){
				cout << "Pong ##############################" << endl;
				In2 = true;
			}

			cout << "In[1,2] = " << In1 << In2 << endl;
			loop();
			cout << "Out[1,2,3,4] = " << *Out1 << *Out2 << *Out3 << *Out4 << endl;



			In1=false;
			In2=false;
			cout << endl;
		}
	}
	return 0;
}
